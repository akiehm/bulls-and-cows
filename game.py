from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    my_list = []
    while len(my_list) < 4:
        number = randint(0,9)
        if number not in my_list:
            my_list.append(number)
    return my_list

# Converts guess to a list of ints
def convert_to_num(strguess):
    number_list = []
    for letter in strguess:
        number_list.append(letter)
    return number_list

#Counts exact matches between converted guess and real answer
def count_exact_matches(firstlist, secondlist):
    count = 0
    for a, b in zip(firstlist, secondlist):
        if a == b:
            count = count + 1
    return count

#Count matches not in exact order between two lists
def count_common_nums(first, second):
    count = 0
    for entry in first:
        if entry in second:
            count = count + 1
    return count
    

def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20
    print(secret)

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        while len(guess) != 4:
            print("Please enter a four-digit number")
            guess = input(prompt)

        converted_guess = convert_to_num(guess)

        total_exact_matches = count_exact_matches(converted_guess, secret)

        total_common_nums = count_common_nums(converted_guess, secret)

        if total_exact_matches == 4:
            print("You got it right!!!")
            return

        print("Total number of bulls: ", total_exact_matches)
        print("Total number of cows: ", total_common_nums - total_exact_matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("You lose. The secret was: ", secret)


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    response = input("Do you want to play a game?")
    while response == "Y":
        play_game()
        response = input("Do you want to play a game?")



if __name__ == "__main__":
    run()
